import unittest
from main import mainwindow
import pygame
import time


class TestShiftOperations(unittest.TestCase):

    def setUp(self):
        self.windowinstance = mainwindow(False)

    def test_left_double_merge(self):
        self.windowinstance.grid = [[2, 0, 2, 4],
                                    [2, 16, 4, 2],
                                    [0, 0, 0, 0],
                                    [0, 0, 0, 0]]
        self.windowinstance.left_shift()
        self.assertEqual(self.windowinstance.grid, [[4, 4, 0, 0],
                                                    [2, 16, 4, 2],
                                                    [0, 0, 0, 0],
                                                    [0, 0, 0, 0]])

    def test_gameover(self):
        self.windowinstance.grid = [[16, 8, 4, 8],
                                    [8, 4, 8, 16],
                                    [0, 8, 16, 32],
                                    [16, 128, 64, 16]]
        self.windowinstance.move("up")
        self.windowinstance.update()
        time.sleep(5)

    def test_color_palate(self):
        step = self.windowinstance.width // 11
        for i in range(0, self.windowinstance.width, step):
            myrect = pygame.Rect(i, 0, step, self.windowinstance.height)
            mycolor = self.windowinstance.gradiant(float(i) /
                                                    self.windowinstance.width)

            self.windowinstance.mainsurf.fill(mycolor, rect=myrect)
        self.windowinstance.draw()
        #leave pattern on screen for visual confirmation
        time.sleep(5)

    def test_tiles(self):
        """
        display number tiles on the screen to confirm legibility and appearance
        """
        for i in range(self.windowinstance.hival):
            column = i % 4
            row = i // 4
            self.windowinstance.grid[row][column] = 2 ** (i + 1)
        self.windowinstance.update()
        time.sleep(5)
if __name__ == '__main__':
    unittest.main()
