#!/usr/bin/python3

import pygame
from pygame import Color, display, event, key, font, Surface
from random import randint, random
from math import floor, log


def main():
    """
    empty docstring... for now
    """
    mywindow = mainwindow()
    mywindow.mainloop()


class mainwindow():
    """
    The main pygame window and internal state required to play 2048,
    handle keypresses, and draw, and update the gameboard.
    """
    def __init__(self, addtiles=True):
        """
        Initializes pygame, internal data structures.
        addtiles=False is for testing purposes and when set will
        start the game with an empty board rather than two tiles
        """

        #number of tiles in x, y direction on gameboard
        self.numx = 4
        self.numy = 4
        #highest power of 2 permitted on game board
        self.hival = 11

        #display parameters
        self.width = 600    # px
        self.height = 600    # px
        #space between tiles/outer border
        self.padding = 20    # px
        self.tilewidth = int(floor((1.0 / self.numx) * (self.width -
                                            ((self.numx + 1) * self.padding))))
        self.tileheight = int(floor((1.0 / self.numy) * (self.height -
                                            ((self.numy + 1) * self.padding))))

        self.grid = [[0 for i in range(self.numx)] for j in range(self.numy)]

        self.gameover = False

        pygame.init()
        self.mainsurf = display.set_mode((self.width, self.height))

        #offscreen buffer for alpha compositing to main surf
        self.alphablit = Surface.convert_alpha(self.mainsurf)
        self.alphablit.fill(Color(0, 0, 0, 0))
        display.flip()

        #clear all permitted events
        event.set_allowed(None)
        #setup allowed event types to speed things up
        #don't currently care about KEYDOWN
        event.set_allowed((pygame.KEYUP, pygame.QUIT))

        #initialize fonts
        self.myfont = self._initfont(str(2 ** self.hival))

        #a pre-rendered array of surfaces ready to be blitted
        self.tiles = self._inittiles(self.hival)

        #initialize game board
        if(addtiles):
            self.add_tile()
            self.add_tile()
        self.score = 0

        self.emptytile = Surface((self.tilewidth, self.tileheight))
        self.emptytile.fill(Color(200, 200, 200, 255))
        self.update()

    def _initfont(self, text):
        """
        returns a font that will render 2048 to fill a box of the size of the
        tuple stored in size
        """
        fontsize = 24    # pt

        testfont = font.SysFont("couriernew", fontsize, bold=True)
        (w, h) = testfont.size(text)
        widthfactor = float(self.tilewidth) / w
        heightfactor = float(self.tileheight) / h
        if(widthfactor < heightfactor):
            fontsize = fontsize * widthfactor * .9
        else:
            fontsize = fontsize * heightfactor * .9

        myfont = font.SysFont("couriernew", int(fontsize), bold=True)

        return myfont

    def _inittiles(self, highestpower):
        """
        returns an array of pygame.Surface's that are prebuilt tiles
        """
        mysurf = [Surface((self.tilewidth, self.tileheight))
                                                for i in range(highestpower)]
        textcol = Color(200, 200, 200, 255)
        for i in range(highestpower):
            curval = 2 ** (i + 1)
            curcolor = self.gradiant(float(i) / highestpower)
            fontsurf = self.myfont.render(str(curval),
                                            True,
                                            textcol)
            w = fontsurf.get_width()
            h = fontsurf.get_height()
            W = mysurf[i].get_width()
            H = mysurf[i].get_height()
            dest = (W // 2 - w // 2, H // 2 - h // 2)
            mysurf[i].fill(curcolor)
            mysurf[i].blit(fontsurf, dest)
        return mysurf

    def mainloop(self):
        """
        main loop. Starts game and handles events
        """
        curevent = None
        while True:
            curevent = event.wait()
            if curevent.type == pygame.KEYUP:
                self.move(key.name(curevent.key))
                self.update()
                if (self.gameover is True and
                            key.name(curevent.key) == "return"):
                    display.quit()
                    #print((self.grid))
                    return
            elif curevent.type == pygame.QUIT:
                display.quit()
                return

    def move(self, direction):
        """
        compresses tiles left, merging appropriate tiles.

        This is achieved by transposing and reflecting grid
        into appropriate orientation, then shifting left,
        then reversing any transpose/reflects that were performed.
        """
        if direction == "left":
            #print("Shifting left...")
            turn = self.left_shift()
        elif direction == "right":
            #print("Shifting right...")
            self.reflect()
            turn = self.left_shift()
            self.reflect()
        elif direction == "up":
            #print("Shifting up...")
            self.transpose()
            turn = self.left_shift()
            self.transpose()
        elif direction == "down":
            #print("Shifting down...")
            self.transpose()
            self.reflect()
            turn = self.left_shift()
            self.reflect()
            self.transpose()
        else:
            return
        #check for changes, if changed, then addtile
        if turn[1] is True:
            self.add_tile()
            self.score += turn[0]

    def reflect(self):
        """
        reflects grid left to right
        """

        for row in self.grid:
            row.reverse()

    def transpose(self):
        """
        transposes columns and rows
        """
        self.grid

        self.grid = [list(i) for i in zip(*self.grid)]

    def left_shift(self):
        """
        Performs left shift and compress on grid
        """
        score = 0
        change = False
        for i in range(self.numy):
            for j in range(self.numx):
                for k in range(j + 1, self.numx):
                    if self.grid[i][j] == 0 and self.grid[i][k] != 0:
                        #print(("move", (i, j, k)))
                        self.grid[i][j] = self.grid[i][k]
                        self.grid[i][k] = 0
                        change = True
                    if (self.grid[i][j] == self.grid[i][k] and
                                            self.grid[i][j] != 0):
                        #print(("merge", (i, j, k)))
                        self.grid[i][j] += self.grid[i][k]
                        score += self.grid[i][j]
                        self.grid[i][k] = 0
                        change = True
                        break
                    if (self.grid[i][j] != 0 and self.grid[i][k] != 0):
                        break
        return (score, change)

    def add_tile(self):
        """
        Adds a tile to the grid in a random location with a randomized value
        """
        vacancies = self.get_vacant()

        location = randint(0, len(vacancies) - 1)
        row, column = vacancies[location]

        if random() < 0.9:
            self.grid[row][column] = 2
        else:
            self.grid[row][column] = 4
        #print(("Added tile at ({},{})".format(row, column)))

    def get_vacant(self):
        """
        builds a list of the locations of vacant spots
        """
        vacancies = []
        #Iterate through grid
        for (rownum, row) in enumerate(self.grid):
            for (colnum, data) in enumerate(row):
                if data == 0:
                    vacancies.append((rownum, colnum))
        return vacancies

    def legalmoves(self):
        """
        Builds and returns a list of legal moves for the current
        gameboard.

        Used to determine game over.
        """

        legalmoves = {"up": 0, "down": 0, "right": 0, "left": 0}

        for rownum, row in enumerate(self.grid):
            for colnum, data in enumerate(row):
                if data == 0:
                    #blank tiles can't move
                    continue

                #check this tile for up moves
                if rownum != 0:
                    if (self.grid[rownum - 1][colnum] == data or
                                self.grid[rownum - 1][colnum] == 0):
                        legalmoves["up"] += 1

                #check this tile for down moves
                if rownum < self.numy - 1:
                    if (self.grid[rownum + 1][colnum] == data or
                                self.grid[rownum + 1][colnum] == 0):
                        legalmoves["down"] += 1

                #check this tile for left moves
                if colnum != 0:
                    if (self.grid[rownum][colnum - 1] == data or
                                self.grid[rownum][colnum - 1] == 0):
                        legalmoves["left"] += 1
                #check this tile for right moves
                if colnum < self.numx - 1:
                    if (self.grid[rownum][colnum + 1] == data or
                                self.grid[rownum][colnum + 1] == 0):
                        legalmoves["right"] += 1
                #check this tile for A-Start moves
                #Labyrinth Zone!!
        return legalmoves

    def gridtopix(self, row, col):
        """
        converts grid coordinates (row,col) to top left corner of tiles in
        display coordinates
        """
        ypix = self.padding + row * (self.padding + self.tileheight)
        xpix = self.padding + col * (self.padding + self.tilewidth)
        return (xpix, ypix)

    def update(self):
        """
        updates display and draws current gameboard.
        """
        self.mainsurf.fill(Color(75, 75, 75, 255))

        for rownum, row in enumerate(self.grid):
            for colnum, data in enumerate(row):
                #draw empty tile
                if(data == 0):
                    #print("here- empty")
                    self.mainsurf.blit(self.emptytile,
                                        self.gridtopix(rownum, colnum))
                else:

                    tilenum = int(floor(log(data) / log(2)) - 1)
                    #print("here - tilenum {}".format(tilenum))
                    self.mainsurf.blit(self.tiles[tilenum],
                                        self.gridtopix(rownum, colnum))

        if(sum(self.legalmoves().values()) == 0):
            self.gameover = True
            endgame = self.myfont.render("GAME OVER", True,
                                                Color(255, 255, 255, 255))
            w = endgame.get_width()
            h = endgame.get_height()
            W = self.mainsurf.get_width()
            H = self.mainsurf.get_height()
            x = (W // 2) - (w // 2)
            y = (H // 2) - (h // 2)

            #fill in transluscent background
            self.alphablit.fill(Color(200, 200, 200, 255 // 2))

            #render GAME OVER
            self.alphablit.blit(endgame, (x, y))

        # blit alpha buffer to display. Without this code, transparent
        # overlays (like game-over screen above) are not possible, and
        # fill in as fully opaque
        self.mainsurf.blit(self.alphablit, (0, 0))
        self.draw()

    def draw(self):
        display.flip()

    def gradiant(self, x):
        """
        Picks a color selected from a smooth gradient, the current hard-coded
        gradient appears something like the gradient named "Incandescent" in
        the GIMP.

        x is a float in the range [0, 1] inclusive.

        x = 0

        Returns a pygame color instance with full opacity (alpha = 255)
        """
        if x <= 0.5:
            red = 500 * x
            green = 0
            blue = 0
        elif x > 0.5 and x <= 0.87:
            red = 250
            green = 250 / (0.87 - 0.5) * (x - 0.5)
            blue = 0
        elif x <= 1:
            red = 250
            green = 250
            blue = 250 / (1 - 0.87) * (x - 0.87)
        return Color(int(red), int(green), int(blue), 255)


if __name__ == "__main__":
    main()
